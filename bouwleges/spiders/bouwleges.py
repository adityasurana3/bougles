import scrapy
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import Select
import time
import re
from ..items import BouwlegesItem
import os
import json


class BouwlegesSpider(scrapy.Spider):
    name = 'bouwleges'
    start_urls = ['https://www.bouwleges.nl/bouwleges/calculate.aspx']
    
    def __init__(self):
        self.options = Options()
        self.options.add_argument("--disable-extensions")
        self.options.add_argument("--disable-gpu")
        self.options.add_argument("--no-sandbox")
        self.options.add_argument('--disable-dev-shm-usage')
        self.options.add_argument('--headless')
        self.driver = webdriver.Chrome(chrome_options=self.options,executable_path='/usr/bin/chromedriver')

    def parse(self, response):
        self.env=json.loads(os.environ.get('SHUB_SETTINGS'))
        USERNAME = self.env.get('project_settings').get('USERNAME1')
        PASSWORD = self.env.get('project_settings').get('PASSWORD')
        BOUWSOM_VALUE = ['100000', '1000000', '10000000', '100000000']
        self.driver.get('http://login.bouwleges.nl/Account/Login?name=Bouwleges')
        self.driver.find_element(By.ID,'UserName').send_keys(USERNAME)
        self.driver.find_element(By.ID,'Password').send_keys(PASSWORD)
        btn_click = self.driver.find_element(By.CLASS_NAME,'btn1')
        btn_click.click()
        time.sleep(2.5)
        
        self.driver.get('http://user.bouwleges.nl/Bouwleges/Calculate.aspx')
        WebDriverWait(self.driver, 10).until(
        EC.presence_of_element_located((By.CLASS_NAME, "Droplist")))
        provincie_value = self.driver.find_elements(By.XPATH,"//select[@id='ctl00_cphContent_dlProvincie']/option")
        provincie_value = [provincie.get_attribute("value") for provincie in provincie_value]

        for i in provincie_value:
            provincie_data_select=Select(self.driver.find_element(By.XPATH,"//select[@id='ctl00_cphContent_dlProvincie']"))
            provincie_data_select.select_by_value(i)
            time.sleep(1.5)
            gemeente_value = self.driver.find_elements(By.XPATH,"//select[@id='ctl00_cphContent_dlGemeente']/option")
            gemeente_value = [gemeente.get_attribute("value") for gemeente in gemeente_value]

            for j in gemeente_value[1:]:
                time.sleep(1.5)
                self.driver.execute_script(f"document.getElementById('ctl00_cphContent_dlGemeente').value = {j};")

                for bouwsom in BOUWSOM_VALUE:
                    time.sleep(1.5)

                    self.driver.execute_script(f"document.getElementById('ctl00_cphContent_tbBouwsom').value = '{bouwsom}';")
                    self.driver.execute_script("document.getElementById('ctl00_cphContent_Button1').disabled = false; ")
                    self.driver.execute_script("document.getElementById('ctl00_cphContent_Button1').click();")
                    time.sleep(1.5)
                    WebDriverWait(self.driver, 10).until(
                    EC.presence_of_element_located((By.ID, "ctl00_cphContent_Titel")))

                    data=self.driver.find_elements(By.XPATH,"//div[@class='leftcolumnbreed']/table/tbody/tr[2]")
                    provincie_gpemeente = [da.text for da in data]
                    provincie_gpemeente = provincie_gpemeente[0].split(',')
                    provincie = provincie_gpemeente[1]
                    gemeente = provincie_gpemeente[0]
                    deel_van_bouwsom_data = self.driver.find_elements(By.XPATH,"//div[@class='leftcolumnbreed']/table/tbody/tr[12]")
                    deel = [deel_van.text for deel_van in deel_van_bouwsom_data]
                    deel_van_bouwsom = deel[0][16:]

                    items = BouwlegesItem()

                    items['provincie'] = provincie
                    items['gemeente'] = gemeente
                    items['deel_van_bouwsom'] = deel_van_bouwsom
                    items['bouwsom'] = bouwsom

                    yield items
                    
                    self.driver.back()