# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy


class BouwlegesItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    provincie = scrapy.Field()
    gemeente = scrapy.Field()
    deel_van_bouwsom = scrapy.Field()
    bouwsom = scrapy.Field()

