# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html


# useful for handling different item types with a single interface
from itemadapter import ItemAdapter
from pydispatch import dispatcher
from scrapy import signals
from scrapy.exporters import CsvItemExporter

from .items import BouwlegesItem
from .settings import NAN

from google.auth import jwt
from google.cloud import pubsub_v1
import base64
import os
import json

# class BouwlegesPipeline:
#     def process_item(self, item, spider):
#         return item

class GCPPipeline(object):
    def __init__(self):
        self.env=json.loads(os.environ.get('SHUB_SETTINGS'))
        self.gcp_creds=self.env.get('project_settings').get('GCP_CREDS')

        self.service_account_info = json.loads(base64.b64decode(self.gcp_creds).decode('utf-8'))
        self.publisher_audience = "https://pubsub.googleapis.com/google.pubsub.v1.Publisher"
        self.credentials = jwt.Credentials.from_service_account_info(
            self.service_account_info, audience=self.publisher_audience
        )
        self.credentials_pub = self.credentials.with_claims(audience=self.publisher_audience)
        self.publisher = pubsub_v1.PublisherClient(credentials=self.credentials_pub)
    def process_item(self, item, spider):
        return item
    
    def close_spider(self, spider):
        job_id = os.environ.get('SHUB_JOBKEY')
        project = 'vf-scrapers'
        message = f"bouwleges,{job_id},{spider.name}"
        self.publisher.publish(f'projects/{project}/topics/bouwleges', bytes(message, 'utf-8'), spam='eggs')

class DefaultValuesPipeline(object):
    """
    Given that we have a definition of the fieds in the items.py, we iterate
    over the fields available for the given item and we set default values.
    """

    def process_item(self, item, spider):
        for _field in item.fields.keys():
            item.setdefault(_field, NAN)
        return item

